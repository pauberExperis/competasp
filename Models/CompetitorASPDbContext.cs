﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitorASP.Models
{
    public class CompetitorASPDbContext:DbContext
    {
        public CompetitorASPDbContext(DbContextOptions<CompetitorASPDbContext> options) : base(options) { }

        public DbSet<Competitor> Competitors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Competitor>().ToTable("Competitor");
        }
    }
}
